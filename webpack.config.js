const path = require("path");

module.exports = {
  mode: "development",
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js"
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: path.resolve(__dirname, "src/")
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, "src/css/")
      }
    ]
  }
};
