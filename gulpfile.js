const gulp = require("gulp");
const sass = require("gulp-dart-sass");

gulp.task("default", () => {
  return gulp
    .src("./src/sass/*.sass")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./dist/css/"));
});
