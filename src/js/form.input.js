module.exports = {
  text: (id, label, value) => {
    return inputText(id, label, value);
  }
};

const inputText = (id, labelText, value) => {
  const label = document.createElement("label");
  const input = document.createElement("input");
  const component = document.createElement("div");
  label.innerText = labelText;
  component.setAttribute("class", "form__input");
  label.setAttribute("for", id);
  label.setAttribute("class", "form__label");
  input.setAttribute("value", value);
  input.setAttribute("id", id);
  const rowLabel = rowForm(label);
  const rowInput = rowForm(input);
  component.appendChild(rowLabel);
  component.appendChild(rowInput);
  return component;
};

const rowForm = child => {
  const row = document.createElement("div");
  row.setAttribute("class", "profile__field");
  row.appendChild(child);
  return row;
};
