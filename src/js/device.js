module.exports = () => {
  const userAgent = navigator.userAgent || navigator.vendor || window.opera;
  let device;

  switch (true) {
    case /android/i.test(userAgent):
    case /iPhone|iPod/.test(userAgent) &&
      !/iPad/.test(userAgent) &&
      !window.MSStream:
    case /windows phone/i.test(userAgent):
      device = "device";
      break;
    default:
      device = "desktop";
  }
  return device;
};
