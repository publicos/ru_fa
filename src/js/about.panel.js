const mobileActions = require("./form.mobile.actions");

module.exports = function(profile, DEVICE_TYPE) {
  //mobile
  if (DEVICE_TYPE == "device") {
    const formEditMobile = () => {
      //get forms
      const forms = document.getElementsByClassName("form");
      //hide info
      const infoForm = forms[0];
      infoForm.setAttribute("class", "form form--hide");
      //show control buttons
      const controlButtons = document.getElementsByClassName("button--hide");
      for (let i = 0; i <= controlButtons.length; i++) {
        controlButtons[0].setAttribute(
          "class",
          `${controlButtons[0]
            .getAttribute("class")
            .replace("button--hide", "")} form__control__button--top`
        );
      }

      //hide edit button
      const buttonEdit = document.getElementById("buttonEditDevice");
      buttonEdit.setAttribute(
        "class",
        `${buttonEdit.getAttribute("class")} button--hide`
      );
      //load form
      if (forms.length < 2) {
        const formMobile = require("./form.mobile")(profile.default);
        infoForm.parentNode.insertBefore(formMobile, infoForm);
      } else {
        forms[1].setAttribute(
          "class",
          `${forms[1]
            .getAttribute("class")
            .replace("form-display", "")} form--hide`
        );
        forms[0].setAttribute(
          "class",
          `${forms[0]
            .getAttribute("class")
            .replace("form--hide", "")} form--display`
        );
      }
    };

    const mobileEditButton = document.createElement("span");
    const icon = document.createElement("i");
    const mobileSaveButton = document.createElement("span");
    const mobileCancelButton = document.createElement("span");
    mobileEditButton.setAttribute(
      "class",
      "button button__edit form__control__button--top"
    );
    mobileEditButton.setAttribute("id", "buttonEditDevice");
    mobileSaveButton.setAttribute("class", "button button--link button--hide");
    mobileCancelButton.setAttribute(
      "class",
      "button button--link button--hide"
    );
    icon.setAttribute("class", "icon ion-md-create");
    mobileEditButton.onclick = formEditMobile;
    mobileCancelButton.onclick = mobileActions["cancel"];
    mobileSaveButton.onclick = mobileActions["save"];
    mobileEditButton.appendChild(icon);
    mobileSaveButton.innerText = "save";
    mobileCancelButton.innerText = "cancel";
    const place = document
      .getElementsByClassName("content__panel--active")[0]
      .getElementsByClassName("panel__title")[0];
    place.appendChild(mobileEditButton);
    place.appendChild(mobileSaveButton);
    place.appendChild(mobileCancelButton);
  }

  //dektop
  [
    { key: "name" },
    { key: "web", icon: "md-globe" },
    { key: "phone", icon: "ios-call" },
    { key: "address", icon: "ios-home" }
  ].map(object => {
    const row = document.createElement("div");
    const field = document.createElement("span");
    const text = document.createElement("span");
    row.setAttribute("class", "form__row");
    field.setAttribute("class", "form__field");
    text.setAttribute("class", "form__field__text");
    text.innerHTML =
      object.key == "name"
        ? `${profile.default["first"]} ${profile.default["last"]}`
        : profile.default[object.key];
    if (object && object.icon) {
      const icon = document.createElement("i");
      icon.setAttribute("class", `icon ion-${object.icon} form__field__icon`);
      field.prepend(icon);
    }
    field.appendChild(text);
    if (DEVICE_TYPE == "desktop") {
      const edit = document.createElement("span");
      const icon = document.createElement("i");
      edit.onclick = () => {
        showEditField(field);
      };
      edit.setAttribute("class", "button button__edit");
      icon.setAttribute("class", "icon ion-md-create");
      edit.appendChild(icon);
      createToolTipForm(field, object.key, text.innerHTML);
      field.appendChild(edit);
    }
    row.appendChild(field);
    document.getElementById("frm").appendChild(row);
  });
};

const createToolTipForm = (field, name, value) => {
  const tooltip = document.createElement("div");
  const title = document.createElement("span");
  const row = document.createElement("div");
  const inputText = document.createElement("input");
  tooltip.setAttribute("class", "tooltip__content");
  title.setAttribute("class", "tooltip__title tooltip__row");
  row.setAttribute("class", "field__row field__row__text tooltip__row");
  inputText.setAttribute("value", value);
  inputText.setAttribute("class", "tooltip__input__text");
  title.innerText = name;
  row.append(inputText);
  tooltip.appendChild(title);
  tooltip.appendChild(row);
  tooltip.appendChild(tooltipControl(field, inputText));
  field.appendChild(tooltip);
};

const tooltipControl = (field, inputControl) => {
  const control = document.createElement("div");
  const save = document.createElement("span");
  const cancel = document.createElement("span");
  cancel.innerText = "Cancel";
  save.innerText = "Save";
  control.setAttribute("class", "field__row tooltip__row");
  save.setAttribute("class", "button button--solid");
  cancel.setAttribute("class", "button button--border button--border--active");
  save.onclick = () => {
    const headerField = field
      .getElementsByClassName("tooltip__title")[0]
      .innerText.toLowerCase();
    if (["name", "address", "phone"].indexOf(headerField) != -1) {
      document.getElementById(headerField).innerText = inputControl.value;
    }
    field.getElementsByClassName("form__field__text")[0].innerText =
      inputControl.value;
    closeToolTips();
  };
  cancel.onclick = () => {
    closeToolTips();
  };
  control.appendChild(save);
  control.appendChild(cancel);
  return control;
};

const showEditField = field => {
  const prefixClass = "tooltip__content";
  const tooltipsList = document.getElementsByClassName(prefixClass);
  for (let i = 0; i < tooltipsList.length; i++) {
    tooltipsList[i].setAttribute("class", prefixClass);
  }
  const tooltipShow = field.getElementsByClassName(`${prefixClass}`);
  tooltipShow[0].setAttribute("class", `${prefixClass} ${prefixClass}--show`);
};

const closeToolTips = () => {
  const prefixClass = "tooltip__content";
  const tooltipsList = document.getElementsByClassName(prefixClass);
  for (let i = 0; i < tooltipsList.length; i++) {
    tooltipsList[i].setAttribute("class", prefixClass);
  }
};
