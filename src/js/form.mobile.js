const input = require("./form.input");
// import { input } from "./form.input";

module.exports = profile => {
  const form = document.createElement("form");
  form.setAttribute("class", "form form--display");
  ["first", "last", "web", "phone", "address"].map(key => {
    form.appendChild(input.text(`${key}Input`, key, profile[key]));
  });
  return form;
};
