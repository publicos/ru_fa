module.exports = {
  cancel: () => {
    formCancelEvent();
  },
  save: () => {
    formSaveEvent();
    formCancelEvent();
  }
};

const formCancelEvent = () => {
  const buttonsControl = document.getElementsByClassName(
    "form__control__button--top"
  );
  const numButtons = buttonsControl.length;
  for (let i = 0; i < numButtons; i++) {
    if (!/hide/.test(buttonsControl[i].getAttribute("class"))) {
      buttonsControl[i].setAttribute(
        "class",
        `${buttonsControl[i].getAttribute("class")} button--hide`
      );
    }
  }
  const buttonEdit = document.getElementById("buttonEditDevice");
  buttonEdit.setAttribute(
    "class",
    buttonEdit.getAttribute("class").replace("button--hide", "")
  );
  const formPrefix = "form";
  document
    .getElementsByClassName("form--display")[0]
    .setAttribute("class", `${formPrefix} ${formPrefix}--hide`);
  document
    .getElementsByClassName("form--hide")[1]
    .setAttribute("class", `${formPrefix} ${formPrefix}--display`);
};

const formSaveEvent = () => {
  const formInfo = document.getElementsByClassName("form--hide")[0];
  const fields = formInfo.getElementsByClassName("form__field__text");
  fields[0].innerHTML = `${getValue("firstInput")} ${getValue("lastInput")}`;
  fields[1].innerHTML = `${getValue("webInput")} `;
  fields[2].innerHTML = `${getValue("phoneInput")} `;
  fields[3].innerHTML = `${getValue("addressInput")} `;
  [
    { key: "name", index: 0 },
    { key: "phone", index: 2, icon: "ios-call" },
    { key: "address", index: 3, icon: "md-pin" }
  ].map(object => {
    const fieldInfo = document.getElementById(object.key);
    fieldInfo.innerText = fields[object.index].innerHTML;
    if (object.icon) {
      const icon = document.createElement("i");
      icon.setAttribute("class", `icon ion-${object.icon} form__field__icon`);
      fieldInfo.prepend(icon);
    }
  });
  document.getElementById("name").innerText = fields[0].innerHTML;
};

const getValue = id => {
  return document.getElementById(id).value;
};
