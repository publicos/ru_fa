//query DOM
const navButtons = document.getElementsByClassName("nav__button");
const panels = document.getElementsByClassName("content__panel");

//const prefix
const prefixClassPanel = "content__panel";

//actions
for (let i = 0; i < navButtons.length; i++) {
  navButtons[i].onclick = e => {
    resetNavButtons();
    navButtons[i].setAttribute("class", "nav__button nav__button--active");
    hideActivePanel();
    panels[i].setAttribute(
      "class",
      `${prefixClassPanel} ${prefixClassPanel}--active`
    );
  };
}

//functions
const resetNavButtons = () => {
  for (let i = 0; i < navButtons.length; i++) {
    navButtons[i].setAttribute("class", "nav__button");
  }
};

const hideActivePanel = () => {
  const active = document.getElementsByClassName(`${prefixClassPanel}--active`);
  active[0].setAttribute(
    "class",
    `${prefixClassPanel} ${prefixClassPanel}--hide`
  );
};
