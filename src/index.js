import profile from "./mock/profile.mock";
import device from "./js/device";

require("./js/tabs.actions");

const DEVICE_TYPE = device();

if (DEVICE_TYPE == "device") {
  //body style
  document.body.setAttribute("class", "body--mobile");

  //button log mobile
  const buttonLogOut = document.getElementById("logOut");
  buttonLogOut.setAttribute(
    "class",
    `${buttonLogOut
      .getAttribute("class")
      .replace("button--border--active", "")} button--link`
  );
  //central header
  const prefixHeaderCentral = "header__central";
  document
    .getElementsByClassName(prefixHeaderCentral)[0]
    .setAttribute("class", `${prefixHeaderCentral}--mobile`);
  //reviews
  const prefixReview = "profile__reviews";
  document
    .getElementsByClassName(prefixReview)[0]
    .setAttribute("class", `${prefixReview} ${prefixReview}--mobile`);

  //remove container class for mobiles
  const container = document.getElementsByClassName("container");
  const prefixContainer = "container";
  container[0].setAttribute(
    "class",
    container[0].getAttribute("class").replace(prefixContainer, "")
  );
  container[0].setAttribute(
    "class",
    `${container[0]
      .getAttribute("class")
      .replace(prefixContainer, "")} ${prefixContainer}__about--mobile`
  );

  document.getElementById("buttonCover").remove();
}

//profile header
[
  { key: "name" },
  { key: "address", icon: "md-pin" },
  { key: "phone", icon: "ios-call" }
].map(obj => {
  const field = document.getElementById(obj.key);
  const info = document.createElement("span");
  info.innerText =
    obj.key == "name"
      ? `${profile.default["first"]} ${profile.default["last"]}`
      : profile.default[obj.key];
  if (obj && obj.icon) {
    const icon = document.createElement("i");
    icon.setAttribute("class", `icon ion-${obj.icon} form__field__icon`);
    field.appendChild(icon);
  }
  field.appendChild(info);
  if (obj.key == "name" && DEVICE_TYPE == "device") {
    field.setAttribute(
      "class",
      "profile__field profile__header--4 header__title--mobile"
    );
    field.innerText = `${profile.default["first"]} ${profile.default["last"]}`;
  }
});

//reviews and rate
const fieldReviews = document.getElementById("reviews");
fieldReviews.innerText = `${profile.default["reviews"]} reviews`;
const fieldRate = document.getElementById("rate");
do {
  const starRate = document.createElement("i");
  const prefixIcon = "icon ion-md-";
  if (profile.default["rate"] >= 2) {
    starRate.setAttribute("class", `${prefixIcon}star`);
  } else if (profile.default["rate"] == 1) {
    starRate.setAttribute("class", `${prefixIcon}star-half`);
  } else {
    starRate.setAttribute("class", `${prefixIcon}star-outline`);
  }
  fieldRate.appendChild(starRate);
  profile.default["rate"] = profile.default["rate"] - 2;
} while (fieldRate.querySelectorAll("i").length < 5);

//showing followers
const iconAdd = document.createElement("i");
iconAdd.setAttribute("class", "icon ion-md-add-circle");
const fieldFollowers = document.createElement("span");
fieldFollowers.appendChild(iconAdd);
fieldFollowers.innerText = `${profile.default["followers"]} followers`;
fieldFollowers.prepend(iconAdd);
const navbarComponent = document.getElementsByClassName("nav__bar")[0];
if (DEVICE_TYPE == "desktop") {
  fieldFollowers.setAttribute("id", "followers");
  navbarComponent.appendChild(fieldFollowers);
} else {
  const followersField = document.createElement("div");
  followersField.setAttribute("class", "profile__column profile__field");
  followersField.appendChild(fieldFollowers);
  navbarComponent.parentNode.insertBefore(followersField, navbarComponent);
}

//about form
require("./js/about.panel")(profile, DEVICE_TYPE);
