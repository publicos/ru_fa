exports.default = {
  first: "Jessica",
  last: "Parker",
  address: "Newport Beach, CA",
  phone: "(949) 325 - 68594",
  web: "www.seller.com",
  rate: 8,
  reviews: 6,
  followers: 15
};
