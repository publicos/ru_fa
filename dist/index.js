/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _mock_profile_mock__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mock/profile.mock */ \"./src/mock/profile.mock.js\");\n/* harmony import */ var _mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_device__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/device */ \"./src/js/device.js\");\n/* harmony import */ var _js_device__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_device__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\n__webpack_require__(/*! ./js/tabs.actions */ \"./src/js/tabs.actions.js\");\n\nconst DEVICE_TYPE = _js_device__WEBPACK_IMPORTED_MODULE_1___default()();\n\nif (DEVICE_TYPE == \"device\") {\n  //body style\n  document.body.setAttribute(\"class\", \"body--mobile\");\n\n  //button log mobile\n  const buttonLogOut = document.getElementById(\"logOut\");\n  buttonLogOut.setAttribute(\n    \"class\",\n    `${buttonLogOut\n      .getAttribute(\"class\")\n      .replace(\"button--border--active\", \"\")} button--link`\n  );\n  //central header\n  const prefixHeaderCentral = \"header__central\";\n  document\n    .getElementsByClassName(prefixHeaderCentral)[0]\n    .setAttribute(\"class\", `${prefixHeaderCentral}--mobile`);\n  //reviews\n  const prefixReview = \"profile__reviews\";\n  document\n    .getElementsByClassName(prefixReview)[0]\n    .setAttribute(\"class\", `${prefixReview} ${prefixReview}--mobile`);\n\n  //remove container class for mobiles\n  const container = document.getElementsByClassName(\"container\");\n  const prefixContainer = \"container\";\n  container[0].setAttribute(\n    \"class\",\n    container[0].getAttribute(\"class\").replace(prefixContainer, \"\")\n  );\n  container[0].setAttribute(\n    \"class\",\n    `${container[0]\n      .getAttribute(\"class\")\n      .replace(prefixContainer, \"\")} ${prefixContainer}__about--mobile`\n  );\n\n  document.getElementById(\"buttonCover\").remove();\n}\n\n//profile header\n[\n  { key: \"name\" },\n  { key: \"address\", icon: \"md-pin\" },\n  { key: \"phone\", icon: \"ios-call\" }\n].map(obj => {\n  const field = document.getElementById(obj.key);\n  const info = document.createElement(\"span\");\n  info.innerText =\n    obj.key == \"name\"\n      ? `${_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"first\"]} ${_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"last\"]}`\n      : _mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[obj.key];\n  if (obj && obj.icon) {\n    const icon = document.createElement(\"i\");\n    icon.setAttribute(\"class\", `icon ion-${obj.icon} form__field__icon`);\n    field.appendChild(icon);\n  }\n  field.appendChild(info);\n  if (obj.key == \"name\" && DEVICE_TYPE == \"device\") {\n    field.setAttribute(\n      \"class\",\n      \"profile__field profile__header--4 header__title--mobile\"\n    );\n    field.innerText = `${_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"first\"]} ${_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"last\"]}`;\n  }\n});\n\n//reviews and rate\nconst fieldReviews = document.getElementById(\"reviews\");\nfieldReviews.innerText = `${_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"reviews\"]} reviews`;\nconst fieldRate = document.getElementById(\"rate\");\ndo {\n  const starRate = document.createElement(\"i\");\n  const prefixIcon = \"icon ion-md-\";\n  if (_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"rate\"] >= 2) {\n    starRate.setAttribute(\"class\", `${prefixIcon}star`);\n  } else if (_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"rate\"] == 1) {\n    starRate.setAttribute(\"class\", `${prefixIcon}star-half`);\n  } else {\n    starRate.setAttribute(\"class\", `${prefixIcon}star-outline`);\n  }\n  fieldRate.appendChild(starRate);\n  _mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"rate\"] = _mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"rate\"] - 2;\n} while (fieldRate.querySelectorAll(\"i\").length < 5);\n\n//showing followers\nconst iconAdd = document.createElement(\"i\");\niconAdd.setAttribute(\"class\", \"icon ion-md-add-circle\");\nconst fieldFollowers = document.createElement(\"span\");\nfieldFollowers.appendChild(iconAdd);\nfieldFollowers.innerText = `${_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a.default[\"followers\"]} followers`;\nfieldFollowers.prepend(iconAdd);\nconst navbarComponent = document.getElementsByClassName(\"nav__bar\")[0];\nif (DEVICE_TYPE == \"desktop\") {\n  fieldFollowers.setAttribute(\"id\", \"followers\");\n  navbarComponent.appendChild(fieldFollowers);\n} else {\n  const followersField = document.createElement(\"div\");\n  followersField.setAttribute(\"class\", \"profile__column profile__field\");\n  followersField.appendChild(fieldFollowers);\n  navbarComponent.parentNode.insertBefore(followersField, navbarComponent);\n}\n\n//about form\n__webpack_require__(/*! ./js/about.panel */ \"./src/js/about.panel.js\")(_mock_profile_mock__WEBPACK_IMPORTED_MODULE_0___default.a, DEVICE_TYPE);\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/js/about.panel.js":
/*!*******************************!*\
  !*** ./src/js/about.panel.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const mobileActions = __webpack_require__(/*! ./form.mobile.actions */ \"./src/js/form.mobile.actions.js\");\n\nmodule.exports = function(profile, DEVICE_TYPE) {\n  //mobile\n  if (DEVICE_TYPE == \"device\") {\n    const formEditMobile = () => {\n      //get forms\n      const forms = document.getElementsByClassName(\"form\");\n      //hide info\n      const infoForm = forms[0];\n      infoForm.setAttribute(\"class\", \"form form--hide\");\n      //show control buttons\n      const controlButtons = document.getElementsByClassName(\"button--hide\");\n      for (let i = 0; i <= controlButtons.length; i++) {\n        controlButtons[0].setAttribute(\n          \"class\",\n          `${controlButtons[0]\n            .getAttribute(\"class\")\n            .replace(\"button--hide\", \"\")} form__control__button--top`\n        );\n      }\n\n      //hide edit button\n      const buttonEdit = document.getElementById(\"buttonEditDevice\");\n      buttonEdit.setAttribute(\n        \"class\",\n        `${buttonEdit.getAttribute(\"class\")} button--hide`\n      );\n      //load form\n      if (forms.length < 2) {\n        const formMobile = __webpack_require__(/*! ./form.mobile */ \"./src/js/form.mobile.js\")(profile.default);\n        infoForm.parentNode.insertBefore(formMobile, infoForm);\n      } else {\n        forms[1].setAttribute(\n          \"class\",\n          `${forms[1]\n            .getAttribute(\"class\")\n            .replace(\"form-display\", \"\")} form--hide`\n        );\n        forms[0].setAttribute(\n          \"class\",\n          `${forms[0]\n            .getAttribute(\"class\")\n            .replace(\"form--hide\", \"\")} form--display`\n        );\n      }\n    };\n\n    const mobileEditButton = document.createElement(\"span\");\n    const icon = document.createElement(\"i\");\n    const mobileSaveButton = document.createElement(\"span\");\n    const mobileCancelButton = document.createElement(\"span\");\n    mobileEditButton.setAttribute(\n      \"class\",\n      \"button button__edit form__control__button--top\"\n    );\n    mobileEditButton.setAttribute(\"id\", \"buttonEditDevice\");\n    mobileSaveButton.setAttribute(\"class\", \"button button--link button--hide\");\n    mobileCancelButton.setAttribute(\n      \"class\",\n      \"button button--link button--hide\"\n    );\n    icon.setAttribute(\"class\", \"icon ion-md-create\");\n    mobileEditButton.onclick = formEditMobile;\n    mobileCancelButton.onclick = mobileActions[\"cancel\"];\n    mobileSaveButton.onclick = mobileActions[\"save\"];\n    mobileEditButton.appendChild(icon);\n    mobileSaveButton.innerText = \"save\";\n    mobileCancelButton.innerText = \"cancel\";\n    const place = document\n      .getElementsByClassName(\"content__panel--active\")[0]\n      .getElementsByClassName(\"panel__title\")[0];\n    place.appendChild(mobileEditButton);\n    place.appendChild(mobileSaveButton);\n    place.appendChild(mobileCancelButton);\n  }\n\n  //dektop\n  [\n    { key: \"name\" },\n    { key: \"web\", icon: \"md-globe\" },\n    { key: \"phone\", icon: \"ios-call\" },\n    { key: \"address\", icon: \"ios-home\" }\n  ].map(object => {\n    const row = document.createElement(\"div\");\n    const field = document.createElement(\"span\");\n    const text = document.createElement(\"span\");\n    row.setAttribute(\"class\", \"form__row\");\n    field.setAttribute(\"class\", \"form__field\");\n    text.setAttribute(\"class\", \"form__field__text\");\n    text.innerHTML =\n      object.key == \"name\"\n        ? `${profile.default[\"first\"]} ${profile.default[\"last\"]}`\n        : profile.default[object.key];\n    if (object && object.icon) {\n      const icon = document.createElement(\"i\");\n      icon.setAttribute(\"class\", `icon ion-${object.icon} form__field__icon`);\n      field.prepend(icon);\n    }\n    field.appendChild(text);\n    if (DEVICE_TYPE == \"desktop\") {\n      const edit = document.createElement(\"span\");\n      const icon = document.createElement(\"i\");\n      edit.onclick = () => {\n        showEditField(field);\n      };\n      edit.setAttribute(\"class\", \"button button__edit\");\n      icon.setAttribute(\"class\", \"icon ion-md-create\");\n      edit.appendChild(icon);\n      createToolTipForm(field, object.key, text.innerHTML);\n      field.appendChild(edit);\n    }\n    row.appendChild(field);\n    document.getElementById(\"frm\").appendChild(row);\n  });\n};\n\nconst createToolTipForm = (field, name, value) => {\n  const tooltip = document.createElement(\"div\");\n  const title = document.createElement(\"span\");\n  const row = document.createElement(\"div\");\n  const inputText = document.createElement(\"input\");\n  tooltip.setAttribute(\"class\", \"tooltip__content\");\n  title.setAttribute(\"class\", \"tooltip__title tooltip__row\");\n  row.setAttribute(\"class\", \"field__row field__row__text tooltip__row\");\n  inputText.setAttribute(\"value\", value);\n  inputText.setAttribute(\"class\", \"tooltip__input__text\");\n  title.innerText = name;\n  row.append(inputText);\n  tooltip.appendChild(title);\n  tooltip.appendChild(row);\n  tooltip.appendChild(tooltipControl(field, inputText));\n  field.appendChild(tooltip);\n};\n\nconst tooltipControl = (field, inputControl) => {\n  const control = document.createElement(\"div\");\n  const save = document.createElement(\"span\");\n  const cancel = document.createElement(\"span\");\n  cancel.innerText = \"Cancel\";\n  save.innerText = \"Save\";\n  control.setAttribute(\"class\", \"field__row tooltip__row\");\n  save.setAttribute(\"class\", \"button button--solid\");\n  cancel.setAttribute(\"class\", \"button button--border button--border--active\");\n  save.onclick = () => {\n    const headerField = field\n      .getElementsByClassName(\"tooltip__title\")[0]\n      .innerText.toLowerCase();\n    if ([\"name\", \"address\", \"phone\"].indexOf(headerField) != -1) {\n      document.getElementById(headerField).innerText = inputControl.value;\n    }\n    field.getElementsByClassName(\"form__field__text\")[0].innerText =\n      inputControl.value;\n    closeToolTips();\n  };\n  cancel.onclick = () => {\n    closeToolTips();\n  };\n  control.appendChild(save);\n  control.appendChild(cancel);\n  return control;\n};\n\nconst showEditField = field => {\n  const prefixClass = \"tooltip__content\";\n  const tooltipsList = document.getElementsByClassName(prefixClass);\n  for (let i = 0; i < tooltipsList.length; i++) {\n    tooltipsList[i].setAttribute(\"class\", prefixClass);\n  }\n  const tooltipShow = field.getElementsByClassName(`${prefixClass}`);\n  tooltipShow[0].setAttribute(\"class\", `${prefixClass} ${prefixClass}--show`);\n};\n\nconst closeToolTips = () => {\n  const prefixClass = \"tooltip__content\";\n  const tooltipsList = document.getElementsByClassName(prefixClass);\n  for (let i = 0; i < tooltipsList.length; i++) {\n    tooltipsList[i].setAttribute(\"class\", prefixClass);\n  }\n};\n\n\n//# sourceURL=webpack:///./src/js/about.panel.js?");

/***/ }),

/***/ "./src/js/device.js":
/*!**************************!*\
  !*** ./src/js/device.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = () => {\n  const userAgent = navigator.userAgent || navigator.vendor || window.opera;\n  let device;\n\n  switch (true) {\n    case /android/i.test(userAgent):\n    case /iPhone|iPod/.test(userAgent) &&\n      !/iPad/.test(userAgent) &&\n      !window.MSStream:\n    case /windows phone/i.test(userAgent):\n      device = \"device\";\n      break;\n    default:\n      device = \"desktop\";\n  }\n  return device;\n};\n\n\n//# sourceURL=webpack:///./src/js/device.js?");

/***/ }),

/***/ "./src/js/form.input.js":
/*!******************************!*\
  !*** ./src/js/form.input.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\n  text: (id, label, value) => {\n    return inputText(id, label, value);\n  }\n};\n\nconst inputText = (id, labelText, value) => {\n  const label = document.createElement(\"label\");\n  const input = document.createElement(\"input\");\n  const component = document.createElement(\"div\");\n  label.innerText = labelText;\n  component.setAttribute(\"class\", \"form__input\");\n  label.setAttribute(\"for\", id);\n  label.setAttribute(\"class\", \"form__label\");\n  input.setAttribute(\"value\", value);\n  input.setAttribute(\"id\", id);\n  const rowLabel = rowForm(label);\n  const rowInput = rowForm(input);\n  component.appendChild(rowLabel);\n  component.appendChild(rowInput);\n  return component;\n};\n\nconst rowForm = child => {\n  const row = document.createElement(\"div\");\n  row.setAttribute(\"class\", \"profile__field\");\n  row.appendChild(child);\n  return row;\n};\n\n\n//# sourceURL=webpack:///./src/js/form.input.js?");

/***/ }),

/***/ "./src/js/form.mobile.actions.js":
/*!***************************************!*\
  !*** ./src/js/form.mobile.actions.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\n  cancel: () => {\n    formCancelEvent();\n  },\n  save: () => {\n    formSaveEvent();\n    formCancelEvent();\n  }\n};\n\nconst formCancelEvent = () => {\n  const buttonsControl = document.getElementsByClassName(\n    \"form__control__button--top\"\n  );\n  const numButtons = buttonsControl.length;\n  for (let i = 0; i < numButtons; i++) {\n    if (!/hide/.test(buttonsControl[i].getAttribute(\"class\"))) {\n      buttonsControl[i].setAttribute(\n        \"class\",\n        `${buttonsControl[i].getAttribute(\"class\")} button--hide`\n      );\n    }\n  }\n  const buttonEdit = document.getElementById(\"buttonEditDevice\");\n  buttonEdit.setAttribute(\n    \"class\",\n    buttonEdit.getAttribute(\"class\").replace(\"button--hide\", \"\")\n  );\n  const formPrefix = \"form\";\n  document\n    .getElementsByClassName(\"form--display\")[0]\n    .setAttribute(\"class\", `${formPrefix} ${formPrefix}--hide`);\n  document\n    .getElementsByClassName(\"form--hide\")[1]\n    .setAttribute(\"class\", `${formPrefix} ${formPrefix}--display`);\n};\n\nconst formSaveEvent = () => {\n  const formInfo = document.getElementsByClassName(\"form--hide\")[0];\n  const fields = formInfo.getElementsByClassName(\"form__field__text\");\n  fields[0].innerHTML = `${getValue(\"firstInput\")} ${getValue(\"lastInput\")}`;\n  fields[1].innerHTML = `${getValue(\"webInput\")} `;\n  fields[2].innerHTML = `${getValue(\"phoneInput\")} `;\n  fields[3].innerHTML = `${getValue(\"addressInput\")} `;\n  [\n    { key: \"name\", index: 0 },\n    { key: \"phone\", index: 2, icon: \"ios-call\" },\n    { key: \"address\", index: 3, icon: \"md-pin\" }\n  ].map(object => {\n    const fieldInfo = document.getElementById(object.key);\n    fieldInfo.innerText = fields[object.index].innerHTML;\n    if (object.icon) {\n      const icon = document.createElement(\"i\");\n      icon.setAttribute(\"class\", `icon ion-${object.icon} form__field__icon`);\n      fieldInfo.prepend(icon);\n    }\n  });\n  document.getElementById(\"name\").innerText = fields[0].innerHTML;\n};\n\nconst getValue = id => {\n  return document.getElementById(id).value;\n};\n\n\n//# sourceURL=webpack:///./src/js/form.mobile.actions.js?");

/***/ }),

/***/ "./src/js/form.mobile.js":
/*!*******************************!*\
  !*** ./src/js/form.mobile.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const input = __webpack_require__(/*! ./form.input */ \"./src/js/form.input.js\");\n// import { input } from \"./form.input\";\n\nmodule.exports = profile => {\n  const form = document.createElement(\"form\");\n  form.setAttribute(\"class\", \"form form--display\");\n  [\"first\", \"last\", \"web\", \"phone\", \"address\"].map(key => {\n    form.appendChild(input.text(`${key}Input`, key, profile[key]));\n  });\n  return form;\n};\n\n\n//# sourceURL=webpack:///./src/js/form.mobile.js?");

/***/ }),

/***/ "./src/js/tabs.actions.js":
/*!********************************!*\
  !*** ./src/js/tabs.actions.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("//query DOM\nconst navButtons = document.getElementsByClassName(\"nav__button\");\nconst panels = document.getElementsByClassName(\"content__panel\");\n\n//const prefix\nconst prefixClassPanel = \"content__panel\";\n\n//actions\nfor (let i = 0; i < navButtons.length; i++) {\n  navButtons[i].onclick = e => {\n    resetNavButtons();\n    navButtons[i].setAttribute(\"class\", \"nav__button nav__button--active\");\n    hideActivePanel();\n    panels[i].setAttribute(\n      \"class\",\n      `${prefixClassPanel} ${prefixClassPanel}--active`\n    );\n  };\n}\n\n//functions\nconst resetNavButtons = () => {\n  for (let i = 0; i < navButtons.length; i++) {\n    navButtons[i].setAttribute(\"class\", \"nav__button\");\n  }\n};\n\nconst hideActivePanel = () => {\n  const active = document.getElementsByClassName(`${prefixClassPanel}--active`);\n  active[0].setAttribute(\n    \"class\",\n    `${prefixClassPanel} ${prefixClassPanel}--hide`\n  );\n};\n\n\n//# sourceURL=webpack:///./src/js/tabs.actions.js?");

/***/ }),

/***/ "./src/mock/profile.mock.js":
/*!**********************************!*\
  !*** ./src/mock/profile.mock.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("exports.default = {\n  first: \"Jessica\",\n  last: \"Parker\",\n  address: \"Newport Beach, CA\",\n  phone: \"(949) 325 - 68594\",\n  web: \"www.seller.com\",\n  rate: 8,\n  reviews: 6,\n  followers: 15\n};\n\n\n//# sourceURL=webpack:///./src/mock/profile.mock.js?");

/***/ })

/******/ });